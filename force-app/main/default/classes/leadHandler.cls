/*
        @ Class name        :   leadHandler
        @ Created by        :   Rathan D Rao
        @ Created on        :   17-02-2022
        @ jira ticket       : SLVI -006
        @ Description       :   Multiple Lead Conversion. 
    */
public with sharing class leadHandler {


    /*
        @ Method name        :   getLeadFieldSetMemebers
        @ Created by        :   Rathan D Rao
        @ Created on        :   17-02-2022
        @ jira ticket       : SLVI -006
        @ Description       :   Fieldset to retrieve the particular field. 
    */
    @AuraEnabled
    public static List<fieldWrapper> getLeadFieldSetMemebers( String objectApiName, String fieldsetApiName) // fucntion to retrieve the fields from  fieldsets from the UI 
    {

        List<fieldWrapper> lstFieldWrapper = new List<fieldWrapper>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldsetApiName);

        List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        for(Schema.FieldSetMember objFM : fieldSetMemberList )
        {
            lstFieldWrapper.add( new fieldWrapper(objFM.getLabel(), objFM.getFieldPath(), String.valueOf(objFM.getType()) ) );
        }
 
        return lstFieldWrapper; 
    }

    public class fieldWrapper
    {
        @AuraEnabled public String label {get; set;} 
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String type {get; set;}

        fieldWrapper(){}
        fieldWrapper(String label,String fieldName, String type  )
        {
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
           
        }
    }

    /*
        @ Method name        :   LeadDetails
        @ Created by        :   Rathan D Rao
        @ Created on        :   18-02-2022
        @ jira ticket       : SLVI -006
        @ Description       :   Retrieve lead data based on fieldset. 
    */

    @AuraEnabled
    public static List<Lead> LeadDetails(String fieldSet)
    { 
        try {

            String query = 'SELECT ';
                for(fieldWrapper fR : getLeadFieldSetMemebers('Lead',fieldSet)) 
                {
                    query += fR.fieldName + ', ';
                }
                query += '  Id FROM Lead  WHERE IsConverted=false ';
                System.debug('query ------>'+query );
                List<Lead> lstLead = Database.query(query);  
                System.debug('LEAD VALUES: '+lstLead);
                return  lstLead;     
                

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

     /*
        @ Method name        :   convertedLeads
        @ Created by        :   Rathan D Rao
        @ Created on        :   18-02-2022
        @ jira ticket       : SLVI -006
        @ Description       :   Query to retrieve the converted Leads. 
    */
    @AuraEnabled
    public static List<Lead> convertedLeads()
    {
        List<Lead> convLeads = [SELECT Id, LastName,Company, ConvertedAccount.Name, ConvertedContact.Name, ConvertedOpportunity.Name FROM Lead WHERE IsConverted=true ];
        System.debug('CONVERTED LEADS:'+convLeads);
        return convLeads;
       
    }

    /*
        @ Method name        :   convertMultipleLeads
        @ Created by        :   Rathan D Rao
        @ Created on        :   19-02-2022
        @ jira ticket       : SLVI -006
        @ Description       :   DML To Mass Convert Leads. 
    */
    @AuraEnabled
    public static void convertMultipleLeads(List<Lead> selectedLead)
    {
        System.debug('LEAD IDS: '+selectedLead);
        for(lead leadObj: selectedLead)
        {
            
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadObj.Id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        }
       
    }
	
}
