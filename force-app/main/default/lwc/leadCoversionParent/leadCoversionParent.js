import { LightningElement,api, track } from 'lwc';
import getLeadFieldSetMemebers from "@salesforce/apex/leadHandler.getLeadFieldSetMemebers";
import LeadDetails from "@salesforce/apex/leadHandler.LeadDetails";
import convertedLeads from "@salesforce/apex/leadHandler.convertedLeads";
import convertMultipleLeads from "@salesforce/apex/leadHandler.convertMultipleLeads";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'LEAD NAME', fieldName: 'leadUrl', type: 'url', typeAttributes: { label: { fieldName: "leadName" }, tooltip:"LEAD NAME", target: "_blank" }  },
    { label: 'CONVERTED ACCOUNT NAME', fieldName: 'accUrl', type: 'url', typeAttributes: { label: { fieldName: "accName" }, tooltip:"CONVERTED ACCOUNT NAME", target: "_blank" } },
    { label: 'CONVERTED OPPORTUNITY NAME', fieldName: 'oppUrl', type: 'url',  typeAttributes: { label: { fieldName: "oppName" }, tooltip:"CONVERTED OPPORTUNITY NAME", target: "_blank" }  },
    { label: 'CONVERTED CONTACT NAME', fieldName: 'conUrl',  type: 'url',typeAttributes: { label: { fieldName: "conName" }, tooltip:"CONVERTED CONTACT NAME", target: "_blank" } }
   
];
export default class LeadCoversionParent extends LightningElement {

    @track lstFields =[];
    @track leadValues =[];
    @track selectedLeads = [];
    @track isConverted ;
    @api fieldSetName;
    @track checkedStatus = false;
    selectedRows=[];
    leadId;
    accId;
    conId;
    oppId;
    leadName;
    accName ;
    conName;
    oppName ;
    
    
    objConvertedLeads={};
    @track listOfValues=[];
    objLead ={};

    columns = columns;

    

    connectedCallback()
    {
        this.fieldSetMember();
        this.fetchLeadDetails();
        this.convertedLeadData();
        this.isConverted = false;
    }

    fieldSetMember() { // Function to retrieve the fieldsets
        getLeadFieldSetMemebers({
            objectApiName: 'Lead',
            fieldsetApiName: this.fieldSetName
        }).then(res => {
            this.lstFields = res;
            console.log(res);
            
        }).catch(error => {
            console.error(error);
        })
    }

   

    fetchLeadDetails() // function to retrieve the Lead Details based on fieldset
    {
        LeadDetails({fieldSet: this.fieldSetName}).then( result =>{
             this.leadValues = result;
        }).catch( error =>{

            console.error(error);
        })
    }

    idValue(event) // To return id value of checkbox
    {
        let id = event.currentTarget.dataset.id;
        console.log("ID VALUE: "+id);
    }

    selectAllRows(event) { // Function to select all checkbox of the rows when the checkbox of the column is selected
        let selectedRows = this.template.querySelectorAll('lightning-input');
       

        for(let i = 0; i < selectedRows.length; i++) {
            if(selectedRows[i].type === 'checkbox') {
                selectedRows[i].checked = event.target.checked;
            }
        }
    }

    convertLeads(event) // Function to convert the selected leads
    {
        let buttonLabel = event.target.label;
        if( buttonLabel == "CONVERT LEADS")
        {
       
        this.selectedRows = [...this.template.querySelectorAll('lightning-input')].filter(element => element.checked)
        console.log("SELECTED ROWS: "+JSON.stringify(this.selectedRows))
        
            for( var i = 0 ;i < this.selectedRows.length; i++)
            {
                if(this.selectedRows[i].checked && this.selectedRows[i].type =="checkbox")
                {
                    console.log('inside if');
                    this.selectedLeads.push({Id: selectedRows[i].dataset.id})
                    console.log('this.selectedLeads: '+this.selectedLeads);
                    this.multipleLeadConversion();
                    this.isConverted = true;
                    var leadLength = this.selectedLeads.length;
                    this.showSuccessToastMessage('SUCCESS', `${leadLength} LEADS HAVE BEEN CONVERTED`, 'success');
                 }
                 else{
                    console.log("INSIDE ELSE");
                    this.showSuccessToastMessage('ERROR', `PLEASE SELECT THE LEADS TO CONVERT`, 'ERROR');
                 }
                 
                }
                
    
        }
        else if( buttonLabel == "CONVERTED LEADS" )
        {
            this.isConverted = true;
            
        }
        
    }

    leadListTable() // Function to toggle to second section to display the Converted Leads once the Lead is Converted
    {
        // this.navigatetoLeadList();
        this.isConverted = false;
    }

    @track convertedLeadList;
    convertedLeadData()  // Function to fetch the converted Lead Data and store it in a list
    {
        
        convertedLeads().then(result =>{

            this.convertedLeadList = result;
             //console.log("CONVERTED LEADS: "+JSON.stringify(result));
            for( var i=0;i < this.convertedLeadList.length; i++)
            {
                this.leadName = this.convertedLeadList[i].LastName;
                this.accName = this.convertedLeadList[i].ConvertedAccount.Name;
                this.conName = this.convertedLeadList[i].ConvertedContact.Name;
                this.oppName = this.convertedLeadList[i].ConvertedOpportunity.Name;
                this.leadId = this.convertedLeadList[i].Id;
                this.accId = this.convertedLeadList[i].ConvertedAccount.Id;
                this.conId = this.convertedLeadList[i].ConvertedContact.Id;
                this.oppId = this.convertedLeadList[i].ConvertedOpportunity.Id;
                 console.log("ACCOUNT: "+this.leadName);

                this.listOfValues.push({'leadUrl' : "/" + this.leadId ,'leadId': this.leadId,'leadName': this.leadName,'accUrl' : "/" + this.accId, 'accId': this.accId,'accName': this.accName,'conUrl' : "/" + this.conId,'conId': this.conId, 'conName': this.conName,'oppUrl' : "/" + this.oppId,'oppId' : this.oppId, 'oppName': this.oppName});
                console.log("PUSHED DATA: "+JSON.stringify((this.listOfValues)));
                

            }
            console.log('final lead list',JSON.stringify(this.listOfValues));
            

        }
            
            ).catch( error =>{
                console.log(error);
            })
    }

    multipleLeadConversion() // Function to call the Multiple Leads Convertion DML Operation
    {
        convertMultipleLeads({selectedLead : this.selectedLeads}).then( result =>{


        }).catch(error =>{
            console.error(error);
        })
    }

    showSuccessToastMessage(title, message, variant) { // function which carries out the toastMessage operation
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

}