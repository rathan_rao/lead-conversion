import { LightningElement,api, track, wire } from 'lwc';
import getLeadFieldSetMemebers from "@salesforce/apex/leadHandler.getLeadFieldSetMemebers";
import LeadDetails from "@salesforce/apex/leadHandler.LeadDetails";
import convertedLeads from "@salesforce/apex/leadHandler.convertedLeads";
import convertMultipleLeads from "@salesforce/apex/leadHandler.convertMultipleLeads";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';


export default class LeadConversion extends LightningElement {

    @track lstFields =[];
    @track leadValues =[];
    @track selectedLeads = [];
    @track isConverted ;
    @track value;
    @api lead;
    @api field;
      


    connectedCallback()
    {
        console.log("FIELDSET LOADED");
        this.fieldSetMember();
        
        this.isConverted = false;
      /*   this.value = this.lead[this.field.fieldName] */
        // this.fetchLeadDetails();
       
    }
    renderedCallback()
    {
        if(this.isConverted == true)
        {
            this.convertedLeadData();
        }
       
    }

    fieldSetMember() { 
        getLeadFieldSetMemebers({
            objectApiName: 'Lead',
            fieldsetApiName: 'leadFieldSet'
        }).then(res => {
            this.lstFields = res;
            console.log(res);
            this.fetchLeadDetails();
        }).catch(error => {
            console.error(error);
        })
    }

    // @track error;
    // @wire(LeadDetails)
    // leads({ error, data })
    // {
    //     console.log("INSIDE");
    //     if(data)
    //     {
    //         this.leadValues = data;
    //         // console.log("TRACK VALUES: "+JSON.stringify(this.leadValues));
    //         console.log("OUTSIDE HAS OWN PROP");
    //          for(var i =0; this.lstFields.length; i++)
    //          {
    //              console.log("HAS OWN PROPERTY: "+data.hasOwnProperty(this.lstFields[i].fieldName))
    //              if( data.hasOwnProperty(this.lstFields[i].fieldName))
    //              {
    //                 console.log("HAS OWN PROPERTY: "+data.hasOwnProperty(this.lstFields[i].fieldName))
    //              }
    //          }
    //         // this.error = undefined;

    //         // for (let key in data) {
    //         //     this.leadValues.push({value: JSON.stringify(data[key]), key:key});
    //         //  }

           
    //     }
    //     else if(error)
    //     {
    //         this.error = error;
    //         this.error = undefined;
    //     }
    // }

    fetchLeadDetails()
    {
        console.log("FETCH DETAILS CALLED");
        LeadDetails().then( result =>{
             this.leadValues = result;
             console.log('>>>>>>>>>>>>>>>>>>>>this');
             this.value = this.lead[this.field.fieldName]; 
             console.log('>>>>>>>>>>>>>>>>>>>>this.lead[this.field.fieldName];',this.lead[this.field.fieldName]);
            // console.log("LSTFIELDS: "+JSON.stringify(this.leadValues));
            // console.log("RESULT: "+JSON.stringify(result));

            // for(var i =0; i < this.lstFields.length; i++)
            // {
            //     console.log("INSIDE FOR LOOP ");
            //     console.log("HAS OWN PROPERTY STATUS: "+result[i].hasOwnProperty(this.lstFields[i].fieldName));
            //     if( result[i].hasOwnProperty(this.lstFields[i].fieldName))
            //     {
            //        this.leadValues[i].val = result[i][this.lstFields[i].fieldName];
            //        console.log("HAS OWN PROP VALUE: "+result[i][this.lstFields[i].fieldName]);
            //     }
            // }

            
            

        }).catch( error =>{

            console.error(error);
        })
    }

    idValue(event)
    {
        let id = event.currentTarget.dataset.id;
        console.log("ID VALUE: "+id);
    }

    selectAllRows(event) {
        let selectedRows = this.template.querySelectorAll('lightning-input');
       

        for(let i = 0; i < selectedRows.length; i++) {
            if(selectedRows[i].type === 'checkbox') {
                selectedRows[i].checked = event.target.checked;
            }
        }
    }

    convertLeads(event)
    {
        let buttonLabel = event.target.label;
        if( buttonLabel == "CONVERT LEADS")
        {
        this.isConverted = true;
        let selectedRows = this.template.querySelectorAll('lightning-input');

      
            for( var i = 0 ;i < selectedRows.length; i++)
            {
                if(selectedRows[i].checked && selectedRows[i].type =="checkbox")
                {
                    this.selectedLeads.push({Id: selectedRows[i].dataset.id})
                }
    
            }
            var leadLength = this.selectedLeads.length;
            this. multipleLeadConversion();
            this.showSuccessToastMessage('SUCCESS', `${leadLength} LEADS HAVE BEEN CONVERTED`, 'success');
           
            // console.log("ID OF CONVERTED LEADS: "+JSON.stringify(this.selectedLeads));
       
        }
        else if( buttonLabel == "CONVERTED LEADS" )
        {
            this.isConverted = true;
            this.leads;
        }
        
    }

    leadListTable()
    {
        this.isConverted = false;
    }

    @track convertedLeadList;
    convertedLeadData()
    {
        convertedLeads().then(result =>{

            this.convertedLeadList = result;
            // console.log("CONVERTED LEADS: "+JSON.stringify(result));


        }
            
            ).catch( error =>{
                console.log(error);
            })
    }

    multipleLeadConversion()
    {
        convertMultipleLeads({selectedLead : this.selectedLeads}).then( result =>{


        }).catch(error =>{
            console.error(error);
        })
    }

    showSuccessToastMessage(title, message, variant) { // function which carries out the toastMessage operation
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }


}
