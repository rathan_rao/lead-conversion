import { LightningElement,api, track } from 'lwc';
import getLeadFieldSetMemebers from "@salesforce/apex/leadHandler.getLeadFieldSetMemebers";
import LeadDetails from "@salesforce/apex/leadHandler.LeadDetails";

export default class LeadConversionChild extends LightningElement {
    @track value;

    @api rowData;
    @api fieldApi;

    @track lstFields =[];
    renderedCallback()
    {
        this.value = this.rowData[this.fieldApi.fieldName];
    }


}